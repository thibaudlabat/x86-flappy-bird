# x86 Flappy Bird

An attempt to recreate Flappy Bird in x86 ASM, in ASCII Art.

![GIF](img/game.gif)

## TODO :
- Pipe randomization
- Comment
- Refactor : cleaner structures, learn new instructions, etc
- (?) NASM warning : label alone on a line without a colon might be in error 