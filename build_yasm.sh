#!/bin/bash

folder="output"
mkdir -p "$folder" \
	&& yasm -f elf main.asm \
	-o "$folder/asm.o" \
	&& ld -m elf_i386 "$folder/asm.o" -o "$folder/asm" -lc -I /lib/ld-linux.so.2
