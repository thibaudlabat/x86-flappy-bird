%ifndef INCLUDE_FRAMEBUFFER
%define INCLUDE_FRAMEBUFFER

%include "functions.asm"

SECTION .bss
framebuffer: resb (SCREEN_X+1)*SCREEN_Y+1

SECTION .text

fb_show: ; TODO : optimiser ça, faire un appel custom de print
    push eax
    mov eax,framebuffer
    call sprint
    pop eax
    ret

fb_setchar_xy:
; In : al(x), ah(y), bl(char)
    push eax
    push ecx
    push ebp
    push edx
    push ebx

    mov ecx,eax
    xor eax,eax
    mov al,ch ; y
    mov ebp,SCREEN_X+1 ; TODO : apprendre à caster directement une constante vers taille de mon choix...
    mul ebp ; eax = 101*y
    xor ebx,ebx
    mov bl,cl ; ebx=x
    add eax,ebx ; eax=101*y+x

    lea eax, [framebuffer+eax]
    pop ebx
    mov byte[eax], bl

    pop edx
    pop ebp
    pop ecx
    pop eax
    ret

fb_setchar: ; TODO check dépassement ?
; In : eax(indice), , bl(char)
    push ecx
    mov ecx,eax
    lea eax, [framebuffer+ecx]
    mov byte[eax], bl
    pop ecx
    ret



.fb_afficher_boucle:
; In : ebx(nb) eax(char)
    push ebx
.afficher_debut:
    cmp ebx,0
    jz .fin_afficher
    dec ebx
    call print_char
    jmp .afficher_debut
.fin_afficher:
    pop ebx
    ret


fb_reset:
; reset le framebuffer en mettant les lignes horizontales,verticales,
; les \n, et le 0 à la fin (cadre du jeu)
    push eax
    push ebx
    push ecx
    push edx

    mov ecx,0
    .fb_reset_loop1:
    cmp ecx,(SCREEN_X+1)*SCREEN_Y
    je .fb_reset_loop1_end
    mov eax, ecx
    mov ebx, ' '
    call fb_setchar
    inc ecx
    jmp .fb_reset_loop1
    .fb_reset_loop1_end:

    ; \n
    mov ecx,0
    .fb_reset_loop2:
    cmp ecx,SCREEN_Y
    je .fb_reset_loop2_end
    mov al, SCREEN_X ; x
    mov ah,cl   ; y
    mov ebx, 10 ; \n
    call fb_setchar_xy
    inc ecx
    jmp .fb_reset_loop2
    .fb_reset_loop2_end:

    ; lignes verticales
    mov ecx,0
    .fb_reset_loop3:
    cmp ecx,SCREEN_Y
    je .fb_reset_loop3_end
    mov al, SCREEN_X-1 ; x
    mov ah,cl   ; y
    mov ebx, '*' ; \n
    call fb_setchar_xy
    mov al, 0 ; x
    call fb_setchar_xy
    inc ecx
    jmp .fb_reset_loop3
    .fb_reset_loop3_end:

    ; lignes horizontales
    mov ecx,0
    .fb_reset_loop4
    cmp ecx,SCREEN_X
    je .fb_reset_loop4_end
    mov al, cl ; x
    mov ah, 0   ; y
    mov ebx, '*' ; \n
    call fb_setchar_xy
    mov ah,SCREEN_Y-1
    call fb_setchar_xy

    inc ecx
    jmp .fb_reset_loop4
    .fb_reset_loop4_end:

    ; null-terminated-string (sécurité)
    lea eax,[framebuffer + (SCREEN_X+1)*SCREEN_Y]
    mov byte[eax], 0

    pop edx
    pop ecx
    pop ebx
    pop eax
    ret

%endif ; INCLUDE_FRAMEBUFFER
