%ifndef INCLUDE_FUNCTIONS
%define INCLUDE_FUNCTIONS

SECTION .text

quit: ; exit(0);
    mov ebx,0
    mov eax,1
    int 80h

strlen: ; eax=(addr str) --> eax=(str len)
    push ebx ; compteur
    mov ebx,eax
.strlen_nextChar:
    cmp byte[ebx],0h
    jz .strlen_end
    inc ebx
    jmp .strlen_nextChar
.strlen_end:
    sub ebx,eax
    mov eax,ebx
    pop ebx
    ret

sprint:
; sprint(eax:string)
    push edx
    push ecx
    push ebx
    push eax ; backup

    push eax
    call strlen
    mov ebx,eax
    pop eax

    mov edx,ebx ; len
    mov ecx,eax ; *str
    mov ebx,1 ; STDOUT
    mov eax,4 ; SYS_WRITE
    int 80h

    pop eax ; cleanup
    pop ebx
    pop ecx
    pop edx
    ret

sprintLF:
    call sprint
    push eax
    mov eax, 0Ah
    push eax ; <-- vers le stack
    mov eax,esp ; addr du stack
    call sprint
    pop eax ; pop addr stack
    pop eax; cleanup
    ret

print_char: ; print eax as a char
    push edx
    push ecx
    push ebx
    push eax

    mov edx,1 ; len
    mov ecx,esp ; *str ; NB : le stack pointe vers eax...
    mov ebx,1 ; STDOUT
    mov eax,4 ; SYS_WRITE
    int 80h

    pop eax
    pop ebx
    pop ecx
    pop edx
    ret

print_digit_eax:
print_digit : ; affiche un digit 0-9 stocké dans eax
    push eax
    add eax,48 ; '0' ASCII
    call print_char
    pop eax
    ret

print_digit_ebx:
    push eax
    mov eax,ebx
    call print_digit_eax
    pop eax
    ret

print_digit_ecx:
    push eax
    mov eax,ecx
    call print_digit_eax
    pop eax
    ret

print_digit_edx:
    push eax
    mov eax,edx
    call print_digit_eax
    pop eax
    ret

print_LF:
    push eax
    mov eax,0Ah
    call print_char
    pop eax
    ret


print_uint: ; Print an unsigned integer to stdout
    ; Arguments : eax=entier à afficher
    push edx
    push ecx
    push ebx
    push eax ; backup

     ; eax : dividend (low)
    mov ebx, 10 ; divisor
    mov ecx, 0 ; digit count

    .print_uint__div_loop:
    ; eax != 0
    xor edx,edx ; dividend (high)

    div ebx
    inc ecx
    push edx
    cmp eax,0
    jnz .print_uint__div_loop

    .print_uint__print_loop:
    pop eax
    call print_digit
    dec ecx
    cmp ecx,0
    jnz .print_uint__print_loop

    pop eax ; cleanup
    pop ebx
    pop ecx
    pop edx
    ret

read_input: ; read_input(size_limit, *buffer)
; In : eax=size_limit, ebx=buffer
    push edx
    push ecx
    push ebx
    push eax ; backup

    mov     edx, eax        ; number of bytes to read
    mov     ecx, ebx     ; reserved space to store our input (known as a buffer)
    mov     ebx, 0          ; write to the STDIN file
    mov     eax, 3          ; invoke SYS_READ (kernel opcode 3)
    int     80h

    pop eax ; cleanup
    pop ebx
    pop ecx
    pop edx
    ret

SECTION .bss
debug_pause_byte: resb 1
SECTION .text
debug_pause: ; wait for input & discard
    push eax
    push ebx

    mov eax, 1
    xor ebx, ebx
    call read_input

    pop ebx
    pop eax
    ret

print_str_charcodes: ; print charcodes of a string
; In : eax(*str)
    push eax
    call strlen
    mov ecx,-1 ; compteur (-1 car on inc directement)

.print_str_charcodes_debut:
    cmp ecx,eax
    jz .print_str_charcodes_fin

    ; pas de virgule avant premier char
    cmp ecx,-1
    jz .print_str_charcodes_first
    push eax        ; affiche ","
    mov eax,","     ; faut vraiment que je passe à une
    call print_char ; convention d'appel réfléchie
    pop eax         ; ...
.print_str_charcodes_first:

    inc ecx

    push eax
    push ebx
    lea ebx, [buffer+ecx] ; charge le ecx-ème char
    xor eax,eax         ; on clear eax pour utiliser seulement al
    mov al, byte[ebx]   ; on charge le char (8bit)
    call print_uint     ; affichage (arg=eax)
    pop ebx
    pop eax

    jmp .print_str_charcodes_debut

.print_str_charcodes_fin:
    pop eax
    ret

str_to_int:
; In : eax(*str)
; Out : eax(int)
; s'arrête au premier caractère non entier, renvoie 0 par défaut
    push edx
    push ecx
    push ebx
    push ebp ; backup

    mov edx,0
    push edx ; nombre de départ

    mov ebp,eax ; *str
    call strlen ; eax=strlen(eax)
    xor ecx,ecx ; compteur (0)
    mov edx,eax ; strlen

    .str_to_int__loop:
    cmp ch,dl
    je .str_to_int__fin

    xor eax,eax
    mov al,ch
    lea ebx, [ebp+eax]
    xor eax,eax
    mov cl, byte[ebx] ; charge le char

    cmp cl,48 ; <'0'
    jl .str_to_int__fin ; invalid char
    cmp cl,58 ; >'9'
    jg  .str_to_int__fin
    sub cl,48 ; 0-9

    pop eax
    push ebx
    mov ebx, 10
    push edx
    mul ebx
    pop edx
    pop ebx
    push ebx
    xor ebx,ebx
    mov bl, cl
    add eax,ebx
    pop ebx
    push eax

    inc ch
    jmp .str_to_int__loop

    .str_to_int__fin:

    pop eax ; retour

    pop ebp; cleaning up
    pop ebx
    pop ecx
    pop edx
    ret


%endif ; INCLUDE_FUNCTIONS
