; jeu du plus ou du moins

extern usleep
extern signal

%include "functions.asm"
%include "screen.asm"
%include "framebuffer.asm"
%include "pipe.asm"

SCREEN_X equ 100
SCREEN_Y equ 50

SECTION .data
erase_line: db 27,"[2K",0
bird_msg: db "x86 bird ",0
msg: db "Press Ctrl+C to jump, Ctrl+Z to exit.                ",0Ah, 0
lostMessage: db "You lost ! Score : ", 0


SECTION .bss
buffer:     resb    256
playerpos:  resb    2 ; (x,y)
jump:  resb    1 ; bool
lost:  resb    1 ; bool
jumpPower:  resb    1 ; int
playerSkipMove: resb 1 ; bool

SECTION .text
global  _start

afficher_boucle:
; In : ebx(nb) eax(char)
    push ebx
.afficher_debut:
    cmp ebx,0
    jz .fin_afficher
    dec ebx
    call print_char
    jmp .afficher_debut
.fin_afficher:
    pop ebx
    ret

afficher_fond:
    push ecx
    push ebx
    push eax

    mov eax,"*"
    mov ebx,SCREEN_X+2
    call afficher_boucle
    call print_LF

    mov ecx,0
.afficher_fond_loop1:
    cmp ecx,SCREEN_Y
    jz .afficher_fond_loop1_end
    inc ecx
    mov eax,"*"
    call print_char
    mov eax," "
    mov ebx,SCREEN_X
    call afficher_boucle
    mov eax,"*"
    call print_char
    call print_LF
    jmp .afficher_fond_loop1
.afficher_fond_loop1_end:
    mov eax,"*"
    mov ebx,SCREEN_X+2
    call afficher_boucle
    call print_LF

    pop eax
    pop ebx
    pop ecx
    ret

microsleep: ; Sleeps without messing with my beloved stack
; In : eax(time in us)
    push edx
    push ecx
    push ebx
    push eax
    call usleep
    pop eax ; discard
    pop ebx
    pop ecx
    pop edx
    ret

modulus: ; modulo helper
; In : eax(x) ebx(mod)
; Out : eax(x%mod)
    push edx
    xor edx,edx
    div ebx
    mov eax,edx
    pop edx
    ret

player_input:
;mov byte[jump],1 ; DEBUG
cmp byte[jump],1
jz player_input__go
ret
player_input__go:
    mov byte[jump], 0
    push eax
    add byte[jumpPower],3
    pop eax
ret

player_move:
push eax
push ebx
xor eax,eax
xor ebx,ebx
mov al,byte[playerSkipMove]
inc al
mov bl,3 ; modulus
call modulus
mov byte[playerSkipMove],al
pop ebx
pop eax

cmp byte[playerSkipMove],0
je player_move__do
ret

player_move__do:
cmp byte[jumpPower],0
jg player_move__up
inc byte[playerpos+1]
ret
player_move__up:
push eax
mov al,byte[jumpPower]
sub byte[playerpos+1],al
pop eax
dec byte[jumpPower]
ret
   

player_check_collisions:
    push eax
    cmp byte[playerpos+1],SCREEN_Y-1
    ja player_check_collisions__lost

    lea eax, [PIPES] ; pipe 0
    call pipe_detect_collision
    cmp eax,1
    je player_check_collisions__lost

    lea eax, [PIPES+PIPE_size] ; pipe 1
    call pipe_detect_collision
    cmp eax,1
    je player_check_collisions__lost

    lea eax, [PIPES+2*PIPE_size] ; pipe 2
    call pipe_detect_collision
    cmp eax,1
    je player_check_collisions__lost

    pop eax
    ret
    player_check_collisions__lost:
    mov byte[lost] ,1
    pop eax
    ret

global gameLoop
gameLoop:
    mov ecx,0 ; tick count
debut:

    call player_input
    call player_move
    call player_check_collisions
    ; check for loosing
    cmp byte[lost],1
    je gameLoop_end


    call fb_reset ; reset le framebuffer
    call pipes_move ; bouge les tuyaux
    call pipes_print ; affiche les tuyaux

    xor ebx,ebx ; wut

    

    mov ax,[playerpos] ; loads player position
    mov bl, '@' ; player char
    call fb_setchar_xy ;


    call console_full_reset
    call fb_show ; affiche le framebuffer

    mov eax,msg ; message d'aide
    call sprintLF

    mov eax,1000*20 ; 40ms between frames
    call microsleep

    inc ecx
    jmp debut
gameLoop_end:
mov eax,lostMessage
call sprint
mov eax,ecx
call print_uint

; print a line feed
push eax
push 0
mov eax,esp
call sprintLF
pop eax
pop eax

    ret



trap: ; handles the Ctrl+C to jump
    mov byte[jump],1
    ret

_start:
    ; signal(SIGINT,trap)
    call console_reset__bis

    push trap ; cf trap:
    push 2
    call signal

    mov byte[playerpos],5
    mov byte[playerpos+1],SCREEN_Y/2
    mov byte[jumpPower],3

    call pipes_init
    call gameLoop
    call quit


