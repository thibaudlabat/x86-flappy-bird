%ifndef INCLUDE_PIPE
%define INCLUDE_PIPE

%include "functions.asm"
%include "framebuffer.asm"

    ; resw pour word, resb pour byte
struc PIPE; Tuyau (élement du jeu)
    .height resb 1 ; hauteur de la partie basse
    .spacing resb 1 ; espacement
    .x resb 1 ; coordonnée x de la gauche du tuyau
endstruc

PIPES_N equ 3
SECTION .bss
PIPES:     resb   PIPE_size*PIPES_N

SECTION .text

pipe_detect_collision:
; In: eax(PIPE addr)
; Out: eax bool, 1 = collision
push ebx
xor ebx,ebx

mov bl,byte[eax+PIPE.x]
cmp byte[playerpos],bl
jne pipe_detect_collision__ok

mov bl,byte[eax+PIPE.height]
cmp byte[playerpos+1],bl
jna pipe_detect_collision__collides

mov bl,byte[eax+PIPE.height]
add bl,byte[eax+PIPE.spacing]
cmp byte[playerpos+1],bl
ja pipe_detect_collision__collides
jmp pipe_detect_collision__ok

pipe_detect_collision__collides:
mov eax,1
pop ebx
ret

pipe_detect_collision__ok:
mov eax,0
pop ebx
ret

__pipe_set: ; private, set un pipe
; In: al(height), ah(spacing), bl(x)
;     ebp(PIPE addr)
    push ecx
    push ebp

    lea ebp, [ecx+PIPE.height]
    mov byte[ebp], al
    lea ebp, [ecx+PIPE.spacing]
    mov byte[ebp], ah
    lea ebp, [ecx+PIPE.x]
    mov byte[ebp], bl

    pop ebp
    pop ecx
    ret

pipes_init: ; TODO : tous les pipes, espacés
    push eax
    push ebx
    push ecx

    mov al, 25 ; height
    mov ah, 20 ; spacing
    mov bl, 45 ; x
    lea ecx, [PIPES] ; pipe 0
    call __pipe_set

    mov al, 20 ; height
    mov ah, 10 ; spacing
    mov bl, 65 ; x
    lea ecx, [PIPES+PIPE_size] ; pipe 1
    call __pipe_set

    mov al, 30 ; height
    mov ah, 6  ; spacing
    mov bl, 99 ; x
    lea ecx, [PIPES+2*PIPE_size] ; pipe 2
    call __pipe_set

    pop ecx
    pop ebx
    pop eax
    ret

pipes_print: ; TODO
; print tous les pipes valides à l'écran
    push eax
    push ebx
    push ebp
    push ecx

    mov eax,PIPES_N ; nombre de tuyaux

    __pipes_print__loop:
    dec eax ; suivant
    lea ebp, [PIPES+eax*PIPE_size]  ; charge le tuyau
    call __pipe_print                 ; l'affiche
    test eax,eax                    ; fini ?
    jne __pipes_print__loop

    pop ecx
    pop ebp
    pop ebx
    pop eax
    ret


__pipe_print: ; affiche un tuyau
; In : ebp(pipe addr)
    push eax
    push ebx
    push ecx
    push edx
    push ebp

    mov bl, '#' ; char

    mov al, [ebp+PIPE.x]  ; x
    mov ah, 0 ; y

    ; loop pour afficher le bas du tuyau
    mov ch,byte[ebp+PIPE.height] ; pipe height
    __pipe_print_loop1:
    inc ah
    call fb_setchar_xy
    cmp ah,ch
    jne __pipe_print_loop1

    ; pipe spacing
    mov ch,byte[ebp+PIPE.spacing]
    add ah,ch

    ; reste du tuyau
    __pipe_print_loop2:
    inc ah
    call fb_setchar_xy
    cmp ah,SCREEN_Y-2
    jne __pipe_print_loop2


    pop ebp
    pop edx
    pop ecx
    pop ebx
    pop eax
    ret







pipes_move:
; bouge les tuyaux
    push eax
    push ebx
    push ebp
    push ecx

    mov eax,PIPES_N ; nombre de tuyaux

    __pipes_move__loop:
    dec eax ; suivant
    lea ebp, [PIPES+eax*PIPE_size]  ; charge le tuyau
    call __pipe_move                 ; l'affiche
    test eax,eax                    ; fini ?
    jne __pipes_move__loop

    pop ecx
    pop ebp
    pop ebx
    pop eax
    ret


__pipe_move: ; affiche un tuyau
; In : ebp(pipe addr)
    push eax
    push ebx
    push ecx
    push edx
    push ebp

    mov ch,byte[ebp+PIPE.x] ; x
    dec ch ; bouge vers la gauche ; TODO : vitesse dépend des FPS ...

    test ch,ch ; 0 --> reset
    jne __pipe_move_1
    mov ch,SCREEN_X-2
    __pipe_move_1:
    mov byte[ebp+PIPE.x],ch ; x

    pop ebp
    pop edx
    pop ecx
    pop ebx
    pop eax
    ret




%endif ; INCLUDE_PIPE
