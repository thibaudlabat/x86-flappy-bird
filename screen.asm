%ifndef INCLUDE_SCREEN
%define INCLUDE_SCREEN

%include "functions.asm"

SECTION .data
console_reset_cursor_magic: db 0x1b,"[1;1H",0h ; "\033[%d;%dH"
console_reset_magic1: db 0x1b,"[2J",0 ; "\x1B[2J"
console_reset_magic2: db "\e[1;1H\e[2J"

SECTION .text

console_reset_cursor:
    push eax
    mov eax,console_reset_cursor_magic
    call sprint
    pop eax
    ret


console_reset__bis:
    push eax
    mov eax,console_reset_magic1
    call sprint
    pop eax
    ret

console_reset:
    push eax
    mov eax,console_reset_magic2
    call sprint
    pop eax
    ret

console_full_reset:
    call console_reset
    call console_reset_cursor
    
    ret

%endif ; INCLUDE_SCREEN
